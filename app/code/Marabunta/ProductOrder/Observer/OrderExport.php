<?php

namespace Marabunta\ProductOrder\Observer;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Filesystem;

class OrderExport implements ObserverInterface
{
    protected $_request;
    protected $_order;
    protected $_productRepository;
    protected $_scopeConfig;
    protected $_customer;
    protected $_storemanager;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Sales\Model\Order $order,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        Filesystem $filesystem,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\CustomerFactory $customer,
        \Magento\Store\Model\StoreManagerInterface $storemanager,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_customer = $customer;
        $this->_storemanager = $storemanager;
        $this->_request = $request;
        $this->_order = $order;
        $this->_fileFactory = $fileFactory;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->_productRepository = $productRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $post = $this->_request->getPost();

        $orderids = $observer->getEvent()->getOrderIds();

        foreach ($orderids as $orderId) {
            $order = $this->_order->load($orderId);

            $websiteID = $this->_storemanager->getStore()->getWebsiteId();
            $customer = $this->_customer->create()->setWebsiteId($websiteID)->loadByEmail($order->getCustomerEmail());
            $customerId = $customer->getId(); // using this
            $customerPoNumber = $customer->getCustomerNumber(); // using this

            $headers = ['Magento_Account_Number','Back_Office_Account_Number','Transaction_Date','Transaction_Time','Magento_Transaction_Number','Client_Reference_Number','Item_Number','Quantity_Sold','Selling_Price','Discount'];

            $name = strtotime("now");
            $file = 'customorderexport/' . $name . '_detailed_orderexport.csv';
            $this->directory->create('ftp/out/');
            $stream = $this->directory->openFile($file, 'w+');
            $stream->lock();
            $stream->writeCsv($headers);

            $items = $order->getAllItems();
            foreach ($items as $item) {
                $orderdetail['Magento_Account_Number'] = $order->getData('created_at');
                $orderdetail['Order_Number'] = $order->getData('increment_id');
                $orderdetail['Transaction_Date'] = date('Y-m-d', strtotime($order->getCreatedAt()));
                $orderdetail['Transaction_Time'] = date('h:i A', strtotime($order->getCreatedAt()));
                $orderdetail['Magento_Transaction_Number'] = $order->getIncrementId();
                $orderdetail['Client_Reference_Number'] = $customerPoNumber;
                $orderdetail['Quantity_Sold'] = $item->getQtyOrdered();
                $orderdetail['Selling_Price'] = $item->getPrice();
                $orderdetail['Discount'] = $order->getDiscountAmount();
                $stream->writeCsv($orderdetail);
            }

            $stream->unlock();
            $stream->close();
        }
    }
}
