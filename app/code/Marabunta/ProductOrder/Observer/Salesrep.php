<?php

namespace Marabunta\ProductOrder\Observer;

use Magento\Catalog\Model\ProductRepository;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Convert\ConvertArray;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;

class Salesrep implements ObserverInterface
{
    protected $dom;
    protected $_request;
    protected $_order;
    protected $_productRepository;
    protected $_scopeConfig;
    protected $_customer;
    protected $_storemanager;
    protected $messageManager;
    protected $content;
    /**
     * @var FileFactory
     */
    private $_fileFactory;
    /**
     * @var Filesystem\Directory\WriteInterface
     */
    private $directory;

    protected $convertArray;
    protected $file;

    public function __construct(
        ManagerInterface $messageManager,
        RequestInterface $request,
        Order $order,
        FileFactory $fileFactory,
        Filesystem $filesystem,
        ScopeConfigInterface $scopeConfig,
        CustomerFactory $customer,
        StoreManagerInterface $storemanager,
        ProductRepository $productRepository,
        ConvertArray $convertArray,
        \Magento\Framework\Filesystem\Io\File $file,
        \Magento\Framework\Xml\Generator $dom
    ) {
        $this->dom = $dom;
        $this->file = $file;
        $this->messageManager = $messageManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_customer = $customer;
        $this->_storemanager = $storemanager;
        $this->_request = $request;
        $this->_order = $order;
        $this->_fileFactory = $fileFactory;
        $this->directory = $filesystem;
        $this->_productRepository = $productRepository;
        $this->convertArray = $convertArray;
    }

    public function createMyXmlFile($assocArray, $rootNodeName, $filename)
    {
        // ConvertArray function assocToXml to create SimpleXMLElement
        $simpleXmlContents = $this->convertArray->assocToXml($assocArray, $rootNodeName);
        // convert it to xml using asXML() function
        $content = $simpleXmlContents->asXML($filename);
        $inc= $this->getDom()->saveXML($content);
        return $this->file->write($inc, 'var/ftp/out/');
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
//        $post = $this->_request->getPost();

        $invoice = $observer->getEvent()->getInvoice();
        $order = $invoice->getOrder();
//        var_dump($order);
        foreach ($order as $orderId) {
            $order = $this->_order->load($orderId);
            $name = strtotime("now");
            $file = 'od' . $name . '.xml';
            $this->directory->create('var/ftp/out/' . $file);
//            $stream = $this->directory->openFile($file, 'w+');
//            $stream->lock();

            $items = $order->getAllItems();
            foreach ($items as $order) {
                $myArray = [
                    'header' => ['yref'=> $order->getData('increment_id'), 'trdt'=>$order->getData('created_at'), 'cuno'],
                    'lines' => [
                        'line' => [
                            'yref' => $order->getData('increment_id'),
                            'twhl' => "WEB",
                            'ponr' => $order->getIncrementId(),
                            'itno' => $order->getSku(),
                            'ppqt' => $order->getQtyOrdered(),
                            'orty' => "OD",
                        ]
                    ]
                ];
            }

//            // ConvertArray function assocToXml to create SimpleXMLElement
//            $simpleXmlContents = $this->convertArray->assocToXml($myArray, 'od');
//            // convert it to xml using asXML() function
//            $content=$simpleXmlContents->asXML();
//            $this->file->write($file, $content);
//            $stream->unlock();
//            $stream->close();
            /** @var array $myArray */
            $this->createMyXmlFile($myArray, 'od', $file);
        }
    }
}
