<?php

namespace Marabunta\Logs\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Marabunta\Logs\Api\Data;
use Marabunta\Logs\Api\LogsRepositoryInterface;

class LogRepository implements LogsRepositoryInterface
{
    private $resourceLog;

    private $logFactory;

    private $searchResultsFactory;

    private $logCollectionFactory;

    /**
     * LogsRepository constructor.
     * @param ResourceModel\Log $resourceLog
     * @param Data\LogsInterface $logFactory
     * @param Data\LogsSearchResultsInterface $searchResultsFactory
     * @param ResourceModel\Log\CollectionFactory $logCollectionFactory
     */
    public function __construct(
        ResourceModel\Log $resourceLog,
        Data\LogsInterface $logFactory,
        Data\LogsSearchResultsInterface $searchResultsFactory,
        ResourceModel\Log\CollectionFactory $logCollectionFactory
    ) {
        $this->resourceLog = $resourceLog;
        $this->logFactory = $logFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->logCollectionFactory = $logCollectionFactory;
    }

    /**
     * @param Data\LogsInterface $log
     * @return Data\LogsInterface
     * @throws CouldNotSaveException
     */
    public function save(Data\LogsInterface $log)
    {
        try {
            $this->resourceLog->save($log);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $log;
    }

    /**
     * @param $logId
     * @return
     * @throws NoSuchEntityException
     */
    public function getById($logId)
    {
        $log = $this->logFactory->create();
        $this->resourceLog->load($log, $logId);
        if (!$log->getId()) {
            throw new NoSuchEntityException(__('Logs with id "%1" does not exist', $logId));
        }
        return $log;
    }

    /**
     * @param Data\LogsInterface $log
     * @return Data\LogsInterface
     * @throws CouldNotDeleteException
     */
    public function delete(Data\LogsInterface $log)
    {
        try {
            $this->resourceLog->delete($log);
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__($e->getMessage()));
        }
        return $log;
    }

    /**
     * @param $logId
     * @return Data\LogsInterface
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($logId)
    {
        return $this->delete($this->getById($logId));
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var ResourceModel\Log\Collection $logCollection */
        $logCollection = $this->logCollectionFactory->create();

        $this->addFiltersToCollection($searchCriteria, $logCollection);
        $this->addSortOrdersToCollection($searchCriteria, $logCollection);
        $this->addPagingToCollection($searchCriteria, $logCollection);

        $logCollection->load();

        return $this->buildSearchResults($searchCriteria, $logCollection);
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param ResourceModel\Log\Collection $logCollection
     */
    private function addFiltersToCollection(SearchCriteriaInterface $searchCriteria, ResourceModel\Log\Collection $logCollection)
    {
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ?: 'eq';
                $logCollection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param ResourceModel\Log\Collection $logCollection
     */
    private function addSortOrdersToCollection(SearchCriteriaInterface $searchCriteria, ResourceModel\Log\Collection $logCollection)
    {
        foreach ((array)$searchCriteria->getSortOrders() as $sortOrder) {
            $logCollection->addSortOrder($sortOrder->getDirection(), $sortOrder->getField());
        }
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param ResourceModel\Log\Collection $logCollection
     */
    private function addPagingToCollection(SearchCriteriaInterface $searchCriteria, ResourceModel\Log\Collection $logCollection)
    {
        $logCollection->setCurPage($searchCriteria->getCurrentPage());
        $logCollection->setPageSize($searchCriteria->getPageSize());
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @param ResourceModel\Log\Collection $logCollection
     *
     * @return SearchResultsInterface
     */
    private function buildSearchResults(SearchCriteriaInterface $searchCriteria, ResourceModel\Log\Collection $logCollection)
    {
        /** @var SearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($logCollection->getItems());
        $searchResults->setTotalCount($logCollection->getSize());

        return $searchResults;
    }
}
