<?php

namespace Marabunta\Logs\Model\Log;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Marabunta\Logs\Model\ResourceModel\Log\CollectionFactory;

class DataProvider extends AbstractDataProvider
{
    protected $collection;

    protected $_loadedData;

    protected $dataPersistor;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $contactCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $contactCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->_loadedData)) {
            return $this->_loadedData;
        }

        $items = $this->collection->getItems();

        foreach ($items as $log) {
            $this->_loadedData[$log->getId()] = $log->getData();
        }

        $data = $this->dataPersistor->get('marabunta_logs_log');

        if (!empty($data)) {
            $log = $this->collection->getNewEmptyItem();
            $log->setData($data);
            $this->_loadedData[$log->getId()] = $log->getData();
            $this->dataPersistor->clear('marabunta_logs_log');
        }

        return $this->_loadedData;
    }
}
