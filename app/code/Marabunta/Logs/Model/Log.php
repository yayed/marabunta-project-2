<?php

namespace Marabunta\Logs\Model;

use Magento\Framework\Model\AbstractModel;
use Marabunta\Logs\Api\Data\LogsInterface;

class Log extends AbstractModel
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init(\Marabunta\Logs\Model\ResourceModel\Log::class);
    }

    public function getId()
    {
        return $this->_getData(LogsInterface::ID);
    }

    public function getUsername()
    {
        return $this->_getData(LogsInterface::USERNAME);
    }

    public function getTime()
    {
        return $this->_getData(LogsInterface::TIME);
    }
    public function getAction()
    {
        return $this->_getData(LogsInterface::ACTION);
    }

    public function getRemoteAddress()
    {
        return $this->_getData(LogsInterface::IP);
    }
    public function getAgent()
    {
        return $this->_getData(LogsInterface::AGENT);
    }
    public function setId($id)
    {
        return $this->setData(LogsInterface::ID, $id);
    }

    public function setUsername($username)
    {
        return $this->setData(LogsInterface::USERNAME, $username);
    }

    public function setAction($action)
    {
        return $this->setData(LogsInterface::ACTION, $action);
    }

    public function setTime($time)
    {
        return $this->setData(LogsInterface::TIME, $time);
    }
    public function setRemoteAddress($remote)
    {
        return $this->setData(LogsInterface::IP, $remote);
    }
    public function setAgent($browser)
    {
        return $this->setData(LogsInterface::AGENT, $browser);
    }
}
