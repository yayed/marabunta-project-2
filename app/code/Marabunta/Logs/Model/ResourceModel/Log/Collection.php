<?php

namespace Marabunta\Logs\Model\ResourceModel\Log;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Marabunta\Logs\Model;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    protected function _construct()
    {
        parent::_construct();
        $this->_init(Model\Log::class, Model\ResourceModel\Log::class);
    }
}
