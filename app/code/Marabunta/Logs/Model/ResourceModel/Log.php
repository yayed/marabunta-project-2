<?php

namespace Marabunta\Logs\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Marabunta\Logs\Api\Data\LogsInterface;

class Log extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('logs_entity', LogsInterface::ID);
    }
}
