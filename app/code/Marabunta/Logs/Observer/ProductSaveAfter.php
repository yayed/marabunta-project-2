<?php

namespace Marabunta\Logs\Observer;

use Magento\Backend\Model\Auth\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\HTTP\Header;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class ProductSaveAfter implements ObserverInterface
{
    protected $httpHeader;
    private $remoteAddress;
    protected $setup;
    protected $authSession;

    public function __construct(ModuleDataSetupInterface $setup, Session $authSession, RemoteAddress $remoteAddress, Header $httpHeader)
    {
        $this->httpHeader = $httpHeader;
        $this->remoteAddress = $remoteAddress;
        $this->authSession = $authSession;
        $this->setup=$setup;
    }

    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        $users = $this->authSession->getUser();
//        var_dump($users);
        $product = $observer->getEvent();
        $id = $product->getData('increment_id');
        $user=$users->getId();
        $time=date('Y-m-d H:i:s');
        $action=$product->getName();
        $address = $this->remoteAddress->getRemoteAddress();
        $userAgent = $this->httpHeader->getHttpUserAgent();

        $this->setDataInTable($id, $user, $time, $action, $address, $userAgent);
    }

    public function setDataInTable($id, $user, $time, $action, $address, $userAgent)
    {
        $conn = $this->setup->getConnection();

        $tableName = $this->setup->getTable('logs_entity');

        $data =[
            'entity_id' => $id,
            'user' => $user,
            'time' => $time,
            'action' => $action,
            'remote' => $address,
            'browser' => $userAgent
        ];

        $conn->insertMultiple($tableName, $data);

        $this->setup->endSetup();
    }
}
