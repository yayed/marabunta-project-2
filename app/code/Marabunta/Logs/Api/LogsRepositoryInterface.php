<?php

namespace Marabunta\Logs\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface LogsRepositoryInterface
{
    /**
     * @param Data\LogsInterface $log
     * @return Data\LogsInterface
     */

    public function save(Data\LogsInterface $log);

    /**
     * @param int $logId
     * @return Data\LogsInterface
     */
    public function getById($logId);

    /**
     * @param Data\LogsInterface $log
     * @return  Data\LogsInterface
     */
    public function delete(Data\LogsInterface $log);

    /**
     * @param int $logId
     * @return Data\LogsInterface
     */
    public function deleteById($logId);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return Data\LogsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
