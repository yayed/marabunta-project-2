<?php

namespace Marabunta\Logs\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface LogsSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return Marabunta\Logs\Api\Data\LogsInterface[]
     * @api
     */
    public function getItems();

    /**
     * @param Marabunta\Logs\Api\Data\LogsInterface[] $items
     * @return $this
     * @api
     */
    public function setItems(array $items = null);
}
