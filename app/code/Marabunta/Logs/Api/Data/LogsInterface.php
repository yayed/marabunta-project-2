<?php

namespace Marabunta\Logs\Api\Data;

interface LogsInterface
{
    const ID='entity_id';
    const USERNAME = 'user';
    const TIME = 'time';
    const ACTION = 'action';
    const IP="remote";
    const AGENT= "browser";

    /**
     * @return int
     */
    public function getId();
    /**
     * @return int
     */
    public function getUser();

    /**
     * @return string
     */

    public function getTime();

    /**
     * @return string
     */

    public function getAction();

    /**
     * @return mixed
     */
    public function getRemoteAddress();

    /**
     * @return mixed
     */
    public function getAgent();

    /**
     * @param $username
     * @return LogsInterface
     */
    public function setUser($username);

    /**
     * @param $id
     * @return mixed
     */
    public function setId($id);
    /**
     * @param $time
     * @return LogsInterface
     */
    public function setTime($time);

    /**
     * @param $action
     * @return LogsInterface
     */
    public function setAction($action);

    /**
     * @param $remote
     * @return mixed
     */
    public function setRemoteAddress($remote);

    /**
     * @param $browser
     * @return mixed
     */
    public function setAgent($browser);
}
