<?php

namespace Marabunta\Logs\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        /**
         * Create table 'logs_entity'
         */

        if (!$setup->tableExists('logs_entity')) {
            $table = $setup->getConnection()
                ->newTable($setup->getTable('logs_entity'))
                ->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    11,
                    [
                        'nullable' => false,
                        'precision' => '10',
                        'auto_increment' => true,
                        'primary'=>true,
                    ],
                    'Logs Id'
                )
                ->addColumn(
                    'user',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => false,
                    ],
                    'User Name'
                )
                ->addColumn(
                    'time',
                    Table::TYPE_DATETIME,
                    11,
                    [
                        'nullable' => false,
                        'precision' => '10',
                    ],
                    'Date de création'
                )
                ->addColumn(
                    'action',
                    Table::TYPE_TEXT,
                    250,
                    [
                        'nullable' => false,
                        'precision' => '10',
                    ],
                    'Logs Id'
                )
                ->addColumn(
                    'remote',
                    Table::TYPE_TEXT,
                    250,
                    [
                        'nullable' => false,
                        'precision' => '10',
                    ],
                    'Adresse IP'
                )
                ->addColumn(
                    'browser',
                    Table::TYPE_TEXT,
                    250,
                    [
                        'nullable' => false,
                        'precision' => '10',
                    ],
                    'Navigateur'
                )
                ->setComment('Logs Table');
            $setup->getConnection()->createTable($table);
        }
        $setup->endSetup();
    }
}
